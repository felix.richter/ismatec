"""
Gui for basic control of a 4 channel Reglo ICC peristaltic pump
"""

import sys
import logging
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QApplication

from ismatec.peristaltic_pump import RegloICCFourChannel


"""
create a .py from the gui design.ui file
venv\Scripts\pyuic5.exe gui/regloc_icc_4_channel_flow_rate/design.ui -o gui/regloc_icc_4_channel_flow_rate/design.py

"""


from gui.regloc_icc_4_channel_flow_rate import design

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


class RegloICCFlowRateGUI(QtWidgets.QMainWindow,
                          design.Ui_MainWindow,
                          ):
    def __init__(self,
                 pump: RegloICCFourChannel,
                 parent=None):
        super().__init__(parent)
        self.setupUi(self)

        self.pump = pump
        for i in range(1, 5):
            self.pump.set_mode_pump_flow_rate(i)

        self.get_direction()
        self.get_rate()
        self.get_tubing_id()
        self.start_pop_up_message()

    def setupUi(self, MainWindow):
        super().setupUi(self)
        self.start_pushButton.clicked.connect(self.start_pump)
        self.stop_pushButton.clicked.connect(self.stop_pump)
        self.set_clockwise_pushButton.clicked.connect(self.set_direction_clockwise)
        self.set_counterclockwise_pushButton.clicked.connect(self.set_direction_counterclockwise)
        self.set_rate_pushButton.clicked.connect(self.set_rate)
        self.set_tubing_id_pushButton.clicked.connect(self.set_tubing_id)

    def start_pop_up_message(self):
        title = 'Start up'
        message = 'This is a basic GUI to control an ISMATEC Reglo ICC 4 channel peristaltic pump. Do not manually ' \
                  'control the pump while using this GUI'
        create_pop_up_message_box(message=message, window_title=title)

    def gui_pump_channel(self) -> int:
        """Current pump channel selected in the gui"""
        pump_channel = int(self.pump_channel_comboBox.currentText())
        return pump_channel

    def get_direction(self):
        all_clockwise = self.pump.all_clockwise()
        channel_1_clockwise = all_clockwise[0]
        channel_2_clockwise = all_clockwise[1]
        channel_3_clockwise = all_clockwise[2]
        channel_4_clockwise = all_clockwise[3]
        self.pump_channel_1_direction_label.setText('Clockwise' if channel_1_clockwise is True else 'Counterclockwise')
        self.pump_channel_2_direction_label.setText('Clockwise' if channel_2_clockwise is True else 'Counterclockwise')
        self.pump_channel_3_direction_label.setText('Clockwise' if channel_3_clockwise is True else 'Counterclockwise')
        self.pump_channel_4_direction_label.setText('Clockwise' if channel_4_clockwise is True else 'Counterclockwise')

    def set_direction_clockwise(self):
        pump_channel = self.gui_pump_channel()
        self._set_direction_clockwise(pump_channel)

    def set_direction_counterclockwise(self):
        pump_channel = self.gui_pump_channel()
        self._set_direction_counterclockwise(pump_channel)

    def _set_direction_clockwise(self, pump_channel):
        self.pump.set_clockwise(pump_channel)
        self.get_direction()

    def _set_direction_counterclockwise(self, pump_channel):
        self.pump.set_counter_clockwise(pump_channel)
        self.get_direction()

    def get_rate(self):
        flow_rates = self.pump.get_all_flow_rate_ml_min()
        channel_1_flow_rate = str(round(flow_rates[0], 3))
        channel_2_flow_rate = str(round(flow_rates[1], 3))
        channel_3_flow_rate = str(round(flow_rates[2], 3))
        channel_4_flow_rate = str(round(flow_rates[3], 3))
        self.pump_channel_1_flow_rate_label.setText(channel_1_flow_rate)
        self.pump_channel_2_flow_rate_label.setText(channel_2_flow_rate)
        self.pump_channel_3_flow_rate_label.setText(channel_3_flow_rate)
        self.pump_channel_4_flow_rate_label.setText(channel_4_flow_rate)

    def set_rate(self):
        pump_channel = self.gui_pump_channel()
        rate = self.rate_doubleSpinBox.value()
        max_flow_rate = self.pump.get_max_flow_rate(pump_channel)
        if rate > max_flow_rate:
            message = f'A rate of {rate} mL/min if > the max rate of {max_flow_rate} mL/min given the pump tubing'
            title = 'Rate too high'
            create_pop_up_message_box(message=message, window_title=title)
            return
        self.pump.set_flow_rate_ml_min(rate, pump_channel)
        self.get_rate()

    def start_pump(self):
        pump_channel = self.gui_pump_channel()
        logger.debug(f'start pump channel {pump_channel}')
        self.pump.start(pump_channel)
        message = f'Pump channel {pump_channel} started'
        create_pop_up_message_box(message=message, window_title=message)
        if pump_channel == 1:
            self.pump_channel_1_running_label.setText('True')
        if pump_channel == 2:
            self.pump_channel_2_running_label.setText('True')
        if pump_channel == 3:
            self.pump_channel_3_running_label.setText('True')
        if pump_channel == 4:
            self.pump_channel_4_running_label.setText('True')

    def stop_pump(self):
        pump_channel = self.gui_pump_channel()
        logger.debug(f'stop pump channel {pump_channel}')
        self.pump.stop(pump_channel)
        message = f'Pump channel {pump_channel} stopped'
        create_pop_up_message_box(message=message, window_title=message)
        if pump_channel == 1:
            self.pump_channel_1_running_label.setText('False')
        if pump_channel == 2:
            self.pump_channel_2_running_label.setText('False')
        if pump_channel == 3:
            self.pump_channel_3_running_label.setText('False')
        if pump_channel == 4:
            self.pump_channel_4_running_label.setText('False')

    def get_tubing_id(self):
        ids = []
        for i in range(1, 5):
            id = str(self.pump.get_tubing_inner_diameter(i))
            ids.append(id)
        self.pump_pump_channel_1_tubing_id_label.setText(ids[0])
        self.pump_pump_channel_2_tubing_id_label.setText(ids[1])
        self.pump_pump_channel_3_tubing_id_label.setText(ids[2])
        self.pump_pump_channel_4_tubing_id_label.setText(ids[3])

    def set_tubing_id(self):
        pump_channel = self.gui_pump_channel()
        id = self.tubing_id_doubleSpinBox.value()
        self.pump.set_tubing_inner_diameter(id, pump_channel)
        self.get_tubing_id()


def create_pop_up_message_box(
        message: str,
        window_title: str,
):
    '''convenience method for creating a pop up message box, with a message and a window title'''
    logger.debug(f'{message}')
    message_box = QtWidgets.QMessageBox()
    message_box.setIcon(QtWidgets.QMessageBox.Information)
    message_box.setText(f"{message}")
    message_box.setWindowTitle(f"{window_title}")
    message_box.setStandardButtons(QtWidgets.QMessageBox.Ok)
    message_box.exec()


def main():
    port = 'COM7'
    icc = RegloICCFourChannel(device_port=port)

    app = QApplication(sys.argv)
    app.setStyle('Fusion')
    form = RegloICCFlowRateGUI(icc)

    form.show()
    app.exec_()


if __name__ == '__main__':
    main()
